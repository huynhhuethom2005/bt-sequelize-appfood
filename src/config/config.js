import dotenv from 'dotenv'

dotenv.config()
export default {
    db_host: process.env.HOST,
    db_user: process.env.USERNAME,
    db_password: process.env.PASSWORD,
    db_port: process.env.PORT,
    db_database: process.env.DATABASE,
    db_dielect: process.env.DIELECT
}
