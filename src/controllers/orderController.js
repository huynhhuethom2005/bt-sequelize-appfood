import { errorCode, successCode } from "../config/response.js"
import sequelize from "../models/index.js"
import initModels from "../models/init-models.js"

const model = initModels(sequelize)

const getOrder = async (req, res) => {
    try {
        let data = await model.order.findAll()

        successCode(res, data, "Get order success")
    } catch (err) {
        errorCode(res, "Server error: ")
    }
}

const createOrder = async (req, res) => {
    try {
        let dataCreate = req.body
        await model.order.create(dataCreate)

        successCode(res, dataCreate, "Create order success")
    } catch (err) {
        errorCode(res, "Server error: ")
    }
}

export { getOrder, createOrder }
