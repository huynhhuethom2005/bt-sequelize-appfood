import { errorCode, successCode } from "../config/response.js"
import sequelize from "../models/index.js"
import initModels from "../models/init-models.js"

const model = initModels(sequelize)

const getLike = async (req, res) => {
    try {
        let data = await model.like_res.findAll({
            include: ["re", "user"]
        })

        successCode(res, data, "Get like success")
    } catch (err) {
        errorCode(res, "Server error!")
    }
}

const createLike = async (req, res) => {
    try {
        let dataCreate = req.body
        await model.like_res.create(dataCreate)
        successCode(res, dataCreate, "Create like success")
    } catch (err) {
        errorCode(res, "Server error!")
    }
}

const deleteLike = async (req, res) => {
    try {
        let deletePara = req.params
        await model.like_res.destroy({
            where: deletePara
        })

        successCode(res, deletePara, "Delete like success")
    } catch (err) {
        errorCode(res, "Server error!")
    }
}

export { getLike, createLike, deleteLike }
