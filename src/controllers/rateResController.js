import { errorCode, successCode } from "../config/response.js"
import sequelize from "../models/index.js"
import initModels from "../models/init-models.js"

const model = initModels(sequelize)

const getRateRes = async (req, res) => {
    try {
        let data = await model.rate_res.findAll({
            include: ["re", "user"]
        })

        successCode(res, data, "Get rate success")
    } catch (err) {
        errorCode(res, "Server error!")
    }
}

const createRateRes = async (req, res) => {
    try {
        let dataCreate = req.body
        await model.rate_res.create(dataCreate)

        successCode(res, dataCreate, "Create rate success")
    } catch (err) {
        errorCode(res, "Server error!")
    }
}

const getRateUser = async (req, res) => {
    try {
        let { user_id } = req.params
        let data = await model.rate_res.findAll({
            where: { user_id },
            include: ["re", "user"]
        })

        successCode(res, data, "Get rate success")
    } catch (err) {
        errorCode(res, "Server error!")
    }
}

const getRateResId = async (req, res) => {
    try {
        let { res_id } = req.params
        let data = await model.rate_res.findAll({
            where: { res_id },
            include: ["re", "user"]
        })

        successCode(res, data, "Get rate success")
    } catch (err) {
        errorCode(res, "Server error!")
    }
}

export { getRateRes, createRateRes, getRateUser, getRateResId }
