import express from "express"
import { createRateRes, getRateRes, getRateUser, getRateResId } from "../controllers/rateResController.js"

const rateResRouter = express.Router()
rateResRouter.get('/get', getRateRes)
rateResRouter.post('/create', createRateRes)
rateResRouter.get('/get/user/:user_id', getRateUser)
rateResRouter.get('/get/res/:res_id', getRateResId)

export { rateResRouter }
