import express from "express"
import { createLike, deleteLike, getLike } from "../controllers/likeController.js"

const likeRouter = express.Router()
likeRouter.get('/get-like', getLike)
likeRouter.post('/create-like', createLike)
likeRouter.delete('/delete-like/:user_id/:res_id', deleteLike)

export { likeRouter }
