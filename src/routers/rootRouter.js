import express from "express";
import { likeRouter } from "./likeRouter.js";
import { rateResRouter } from "./rateResRouter.js";
import { orderRouter } from "./orderRouter.js";

const rootRouter = express.Router()

rootRouter.use('/like', likeRouter)
rootRouter.use('/rate-res', rateResRouter)
rootRouter.use('/order', orderRouter)

export { rootRouter } 
