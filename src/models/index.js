import { Sequelize } from 'sequelize'
import config from '../config/config.js'

const sequelize = new Sequelize(config.db_database, config.db_user, config.db_password, {
    host: config.db_host,
    port: config.db_port,
    dialect: config.db_dielect
})

export default sequelize

try {
    await sequelize.authenticate()
    console.log('Connect success!')
} catch (err) {
    console.log('err :>> ', err);
}